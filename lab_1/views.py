from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhammad Andriansyah Putra' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 8, 22) #TODO Implement this, format (Year, Month, Date)
npm = 1706044093 # TODO Implement this
college = "Universitas Indonesia"
hobby = "Watching football, watching films, listening to music"
desc = "I am just an average guy who lives his life optimistically and realistically"
fr_name = 'Muhammad Lutfi Arif' # TODO Implement this
fr_birth_date = date(1999, 10, 23) #TODO Implement this, format (Year, Month, Date)
fr_npm = 1706979360 # TODO Implement this
fr_hobby = "Reading novels, sports, gaming, coding"
fr_desc = "Good guy, good friend."
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'college': college, 'hobby': hobby, 'desc': desc, 'fr_name':fr_name, 'fr_age':calculate_age(fr_birth_date.year), 'fr_college':college, 'fr_npm':fr_npm, 'fr_hobby':fr_hobby,'fr_desc':fr_desc}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
