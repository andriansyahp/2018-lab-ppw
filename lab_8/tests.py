# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import hm
# import unittest

# # Create your tests here.
# class Lab8UnitTest(TestCase):
    
#     def test_hello_page_is_exist(self):
#         response = Client().get('/8th_story')
#         self.assertEqual(response.status_code,200)

#     def test_using_hm_func(self):
#         found = resolve('/8th_story')
#         self.assertEqual(found.func, hm)

#     def test_page_contains_welcome_greetings(self):
#         response = Client().get('/8th_story')
#         html_response = response.content.decode('utf8')
#         self.assertIn("Welcome", html_response)      
#         self.assertIn("Selamat Datang", html_response)
    
#     def test_index_contains_buttons_to_change_theme(self):
#         response = Client().get('/8th_story')
#         html_response = response.content.decode('utf8')
#         self.assertIn('theme-select', html_response)

#     def test_page_contains_about_me_and_has_my_age(self):
#         response = Client().get('/8th_story')
#         html_response = response.content.decode('utf8')
#         self.assertIn("Hello", html_response)      
#         self.assertIn("19", html_response)

#     def test_page_contains_cv(self):
#         response = Client().get('/8th_story')
#         html_response = response.content.decode('utf8')
#         self.assertIn("Résumé", html_response)      

#     def test_page_contains_accordion(self):
#         response = Client().get('/8th_story')
#         html_response = response.content.decode('utf8')
#         self.assertIn("accordion", html_response)      

