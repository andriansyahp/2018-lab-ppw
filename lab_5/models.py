from django.db import models

# Create your models here.
class Schedule(models.Model):
    name = models.CharField(max_length=27)
    date = models.DateTimeField(auto_now_add=False)
    title = models.TextField()
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=50)


    def __str__(self):
        return self.title
