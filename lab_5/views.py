from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Activity_Book
from .models import Schedule

def fifth_home(request):
    return render(request, '5th_home.html')

def desc(request):
    return render(request, 'desc.html')

def resume(request):
    return render(request, 'resume.html')

def find_me(request):
    return render(request, 'find_me.html')

def some_form(request):
    return render(request, 'some_form.html')

def activities(request):
    response = {'message_form' : Activity_Book()}
    return render(request, 'activities.html', response)

def message_post(request):
    form = Activity_Book(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response = {'name' : request.POST['name'], 'date' : request.POST['date'],
                    'title' : request.POST['title'], 'place' : request.POST['place'],
                    'category' : request.POST['category'], 'result' : 'Success.'}
        activity = Schedule(name=response['name'], date=response['date'],
                          title=response['title'], place=response['place'],
                          category=response['category'])
        activity.save()
        html ='result_form.html'
        return render(request, html, response)
    else:
        response = {'name' : '???', 'date' : '???',
                    'title' : '???', 'place' : '???',
                    'category' : '???',
                    'result' : 'Failed. \nMaybe you didn\'t use the necessary format. \nDate must be in YYYY-MM-DD HH:mm.'}
        return render(request, 'result_form.html', response)

def message_table(request):
    title = Schedule.objects.all()
    response = {'title' : title}
    html = 'table.html'
    if (request.GET.get('destroy')):
        Schedule.objects.all().delete()
        return render(request, html)
    return render(request, html , response)


# Create your views here.
