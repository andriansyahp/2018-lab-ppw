from django import forms
from .models import Schedule

class Activity_Book(forms.ModelForm):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    def __init__(self, *args, **kwargs):
        super(Activity_Book, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
    class Meta:
        model = Schedule
        fields = ('name', 'date', 'title', 'place', 'category')
