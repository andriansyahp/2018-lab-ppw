from django.urls import path, re_path
from django.conf.urls import url
from . import views
#url for app
urlpatterns = [
    path('', views.fifth_home, name='fifth'),
    path('desc', views.desc, name='desc'),
    path('resume', views.resume, name='resume'),
    path('find_me', views.find_me, name='find_me'),
    path('some_form', views.some_form, name='some_form'),
    path('activities', views.activities, name='activities'),
    path('result_form', views.message_post, name='message_post'),
    path('table', views.message_table, name='message_table'),
]
