"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import include, path
from django.urls import re_path
from django.contrib import admin
from django.conf.urls import url
from main_menu.views import *
from lab_1.views import *
from lab4.views import *
from lab_5.views import *
from lab_6.views import *
from lab_7.views import *
from lab_8.views import *
from lab_9.views import *
from lab_10.views import *
from lab_11.views import *


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main_menu.urls')),
    path('1st_story/', include('lab_1.urls')),
    path('4th_story/', include('lab4.urls')),
    path('5th_story/', include('lab_5.urls')),
    path('6th_story/', include('lab_6.urls')),
    path('7th_story/', include('lab_7.urls')),
    path('8th_story/', include('lab_8.urls')),
    path('9th_story/', include('lab_9.urls')),
    path('10th_story/', include('lab_10.urls')),
    path('11th_story/', include('lab_11.urls')),
]
