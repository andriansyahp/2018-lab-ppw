window.onload = function(){
    document.getElementById("loading").style.display = "none" 
}

$(document).ready(function() {    
    (function(){
        $(".flex-slide").each(function(){
            $(this).hover(function(){
                $(this).find('.flex-title').css({
                    transform: 'rotate(0deg)',
                    top: '10%'
                });
                $(this).find('.flex-about').css({
                    opacity: '1'
                });
            }, function(){
                $(this).find('.flex-title').css({
                    transform: 'rotate(90deg)',
                    top: '15%'
                });
                $(this).find('.flex-about').css({
                    opacity: '0'
                });
            })
        });
    })();

    var acc = document.getElementsByClassName("accordion");

    for (var X = 0; X < acc.length; X++) {
        acc[X].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            } 
        });
        acc[X].addEventListener("hover", function() {
            this.tooltip();
        });
    }

    document.getElementById('blue').onclick = function () { 
        document.styleSheets[1].disabled=false;
	    document.styleSheets[2].disabled=true;
    };

    document.getElementById('pink').onclick = function () { 
        document.styleSheets[2].disabled=false;
	    document.styleSheets[1].disabled=true;
    };
});