window.onload = function(){
    document.getElementById("loading").style.display = "none" 
}

function choose_api(type) {
    var counter = 0;
    $('tbody').html('');
    $('#fav').html(counter);
    $.ajax({
        type : 'GET',
        url : 'books_json/' + type,
        dataType : 'json',
        success : function(data) {
            var response ='';

            for(var i=0; i < data.my_data.length; i++) {
                response += '<tr> <td>' + parseInt(i+1) +'</td> ';
                response += '<td>' + data.my_data[i].book_title + '</td>';
                response += '<td>' + data.my_data[i].book_author + '</td>';
                response += '<td>' + data.my_data[i].book_publishedDate+'</td>';
                response += '<td> <img src="' + data.my_data[i].book_thumbnail+'"></td> ';
                response += '<td>' + data.my_data[i].book_desc+'</td>';
                response += '<td> <button class="btn btn-warning" id="favToggle"><span class="glyphicon glyphicon-star"></span></button></td></tr>';
            }

            $('tbody').html(response);
        }
    });
};

$(document).ready(function (e) {
    var counter = 0;
    $('#fav').html(counter);
    $.ajax({
        type : 'GET',
        url : 'books_json/quilting',
        dataType : 'json',
        success : function(data) {
            var response ='';

            for(var i=0; i < data.my_data.length; i++) {
                response += '<tr> <td>' + parseInt(i+1) +'</td> ';
                response += '<td>' + data.my_data[i].book_title + '</td>';
                response += '<td>' + data.my_data[i].book_author + '</td>';
                response += '<td>' + data.my_data[i].book_publishedDate+'</td>';
                response += '<td> <img src="' + data.my_data[i].book_thumbnail+'"></td> ';
                response += '<td>' + data.my_data[i].book_desc+'</td>';
                response += '<td> <button class="btn btn-warning" id="favToggle"><span class="glyphicon glyphicon-star"></span></button></td></tr>';
            }

            $('tbody').html(response);
        }
    });


    (function(){
        $(".flex-slide").each(function(){
            $(this).hover(function(){
                $(this).find('.flex-title').css({
                    transform: 'rotate(0deg)',
                    top: '10%'
                });

                $(this).find('.flex-about').css({
                    opacity: '1'
                });
            }, function(){
                $(this).find('.flex-title').css({
                    transform: 'rotate(90deg)',
                    top: '15%'
                });

                $(this).find('.flex-about').css({
                    opacity: '0'
                });
            })
        });
    })();

    var acc = document.getElementsByClassName("accordion");

    for (var X = 0; X < acc.length; X++) {
        acc[X].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            } 
        });
        acc[X].addEventListener("hover", function() {
            this.tooltip();
        });
    }

    document.getElementById('blue').onclick = function () { 
        document.styleSheets[1].disabled=false;
	    document.styleSheets[2].disabled=true;
    };

    document.getElementById('pink').onclick = function () { 
        document.styleSheets[2].disabled=false;
	    document.styleSheets[1].disabled=true;
    };

    $(document).on('click', '#favToggle', function() {
        if($(this).hasClass('btn-warning')) {
            $(this).addClass('btn-info');
            $(this).removeClass('btn-warning');
            counter += 1;
        }
        else {
            $(this).addClass('btn-warning');
            $(this).removeClass('btn-info');
            counter -= 1;
        }

        $('#fav').html(counter);
    });
});

