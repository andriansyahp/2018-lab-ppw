from django.urls import path
from . import views
#url for app
urlpatterns = [
    path('', views.view_a, name='fourth'),
    path('about', views.view_b, name='view_b'),
    path('cv', views.view_c, name='view_c'),
    path('contact', views.view_d, name='view_d'),
    path('form', views.view_e, name='view_e'),
]
