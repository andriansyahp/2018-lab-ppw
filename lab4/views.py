from django.shortcuts import render

def view_a(request):
    return render(request, 'home.html')

def view_b(request):
    return render(request, 'about.html')

def view_c(request):
    return render(request, 'cv.html')

def view_d(request):
    return render(request, 'contact.html')

def view_e(request):
    return render(request, 'form.html')
# Create your views here.
