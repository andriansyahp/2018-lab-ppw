from django.urls import path, include
from django.conf.urls import url
from django.contrib.auth import views
from . import views
#url for app
urlpatterns = [
    path('', views.home_eleventh, name='eleventh'),
    path('login', views.login, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('auth', include('social_django.urls', namespace='social')),  # <- Here
    path('booklist', views.list_books, name='lists'),
    path('booklist_data/<type>', views.books_data, name='jsondata'),
]
