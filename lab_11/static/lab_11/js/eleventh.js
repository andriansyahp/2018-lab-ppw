// window.onload = function(){
//     document.getElementById("loading").style.display = "none" 
// }

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
  }

function choose_api(type) {
    var counter = 0;
    $('tbody').html('');
    $('#fav').html(counter);
    $.ajax({
        type : 'GET',
        url : 'booklist_data/' + type,
        dataType : 'json',
        success : function(data) {
            var response ='';

            for(var i=0; i < data.my_data.length; i++) {
                response += '<tr> <td>' + parseInt(i+1) +'</td> ';
                response += '<td>' + data.my_data[i].book_title + '</td>';
                response += '<td>' + data.my_data[i].book_author + '</td>';
                // response += '<td>' + data.my_data[i].book_publishedDate+'</td>';
                // response += '<td> <img src="' + data.my_data[i].book_thumbnail+'"></td> ';
                // response += '<td>' + data.my_data[i].book_desc+'</td>';
                response += '<td> <button class="btn btn-warning" id="favToggle"><span class="glyphicon glyphicon-star"></span></button></td></tr>';
            }

            $('tbody').html(response);
        }
    });
};

$(document).ready(function (e) {
    var counter = 0;
    $('#fav').html(counter);
    $.ajax({
        type : 'GET',
        url : 'booklist_data/quilting',
        dataType : 'json',
        success : function(data) {
            var response ='';

            for(var i=0; i < data.my_data.length; i++) {
                response += '<tr> <td>' + parseInt(i+1) +'</td> ';
                response += '<td>' + data.my_data[i].book_title + '</td>';
                response += '<td>' + data.my_data[i].book_author + '</td>';
                // response += '<td>' + data.my_data[i].book_publishedDate+'</td>';
                // response += '<td> <img src="' + data.my_data[i].book_thumbnail+'"></td> ';
                // response += '<td>' + data.my_data[i].book_desc+'</td>';
                response += '<td> <button class="btn btn-warning" id="favToggle"><span class="glyphicon glyphicon-star"></span></button></td></tr>';
            }

            $('tbody').html(response);
        }
    });


    document.getElementById('blue').onclick = function () { 
        document.styleSheets[1].disabled=false;
	    document.styleSheets[2].disabled=true;
    };

    document.getElementById('pink').onclick = function () { 
        document.styleSheets[2].disabled=false;
	    document.styleSheets[1].disabled=true;
    };

    $(document).on('click', '#favToggle', function() {
        if($(this).hasClass('btn-warning')) {
            $(this).addClass('btn-info');
            $(this).removeClass('btn-warning');
            counter += 1;
        }
        else {
            $(this).addClass('btn-warning');
            $(this).removeClass('btn-info');
            counter -= 1;
        }

        $('#fav').html(counter);
    });
});

