from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
# from django.contrib.auth.views import logout
from django.contrib.auth import logout
import requests, json

# Create your views here.
def home_eleventh(request):
    return render(request, 'landing-page-lab11.html')

def list_books(request):
    return render(request, 'booklists.html')

def login(request):
    return render(request, 'login.html')

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/11th_story') 

def books_data(request, type = None):
    json_books_req = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + str(type))
    json_books_resp = json_books_req.json()
    resp =[]
    for book_details in json_books_resp['items']:
        book_data = {}
        book_data['book_title'] = book_details['volumeInfo']['title']
        book_data['book_author'] = ", ".join(book_details['volumeInfo']['authors'])
        # book_data['book_publishedDate'] = book_details['volumeInfo']['publishedDate']
        # book_data['book_thumbnail'] = book_details['volumeInfo']['imageLinks']['thumbnail']
        # book_data['book_desc'] = book_details['volumeInfo']['description']
        resp.append(book_data)   
    dataJson = json.dumps({"my_data":resp})
    mimetype = 'application/json'
    return HttpResponse(dataJson, mimetype)



