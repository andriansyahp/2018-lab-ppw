from django.urls import path
from django.conf.urls import url
from . import views
#url for app
urlpatterns = [
    path('', views.homee, name='ninth'),
    path('books', views.list_book, name='list'),
    path('books_json/<type>', views.book_data, name='json'),
]
