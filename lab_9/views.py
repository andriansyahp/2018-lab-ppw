from django.shortcuts import render
from django.http import HttpResponse
import requests, json

# Create your views here.
def homee(request):
    return render(request, 'landing-page-lab9.html')

def list_book(request):
    return render(request, 'booklist.html')

def book_data(request, type = None):
    json_books_req = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + str(type))
    json_books_resp = json_books_req.json()
    resp =[]
    for book_details in json_books_resp['items']:
        book_data = {}
        book_data['book_title'] = book_details['volumeInfo']['title']
        book_data['book_author'] = ", ".join(book_details['volumeInfo']['authors'])
        # book_data['book_publishedDate'] = book_details['volumeInfo']['publishedDate']
        # book_data['book_thumbnail'] = book_details['volumeInfo']['imageLinks']['thumbnail']
        # book_data['book_desc'] = book_details['volumeInfo']['description']
        resp.append(book_data)   
    dataJson = json.dumps({"my_data":resp})
    mimetype = 'application/json'
    return HttpResponse(dataJson, mimetype)



