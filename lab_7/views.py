from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import StatusMessage
# Create your views here.
response = {}
def index_7(request):    
    messages = StatusMessage.objects.all()
    response['messages'] = messages
    html = 'landing_page.html'
    response['message_form'] = Status_Form()
    if (request.GET.get('destroy')):
        StatusMessage.objects.all().delete()
        return HttpResponseRedirect('/7th_story', response)
    return render(request, html, response)

def status_send(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['message'] = request.POST['message']
        status = StatusMessage(name=response['name'],message=response['message'])
        status.save()
        return HttpResponseRedirect('/7th_story')
    else:
        return HttpResponseRedirect('/7th_story')
