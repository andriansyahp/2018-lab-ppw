# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import index_7
# from .models import StatusMessage
# from .forms import Status_Form
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import unittest
# import time


# # Create your tests here.
# class Lab7UnitTest(TestCase):
    
#     def test_hello_page_is_exist(self):
#         response = Client().get('/7th_story')
#         self.assertEqual(response.status_code,200)

#     def test_using_index_func(self):
#         found = resolve('/7th_story')
#         self.assertEqual(found.func, index)

#     def test_index_contains_hello_apa_kabar(self):
#         response = Client().get('/7th_story')
#         html_response = response.content.decode('utf8')
#         self.assertIn("Hello", html_response)      
#         self.assertIn("Apa Kabar?", html_response)
    
#     def test_index_contains_form_to_send_status_message(self):
#         response = Client().get('/7th_story')
#         html_response = response.content.decode('utf8')
#         self.assertIn('<form class="post-form" method="POST" action="/7th_story/status_send">', html_response)

#     def test_model_can_create_new_status(self):
#         # Creating a new message
#         new_status = StatusMessage.objects.create(name='budi', message='mengerjakan lab_7 ppw')

#         # Retrieving all available status messages
#         counting_all_available_status = StatusMessage.objects.all().count()
#         self.assertEqual(counting_all_available_status, 1)

#     def test_form_validation_for_blank_items(self):
#         form = Status_Form(data={'name': '', 'message': ''})
#         self.assertFalse(form.is_valid())
#         self.assertEqual(
#             form.errors['message'],
#             ["This field is required."]
#         )

#     def test_lab7_post_success(self):
#         test = 'Anonymous'
#         response_post = Client().post('/7th_story', {'name': test, 'message': test})
#         self.assertEqual(response_post.status_code, 200)


# # Create your tests here.
# class Lab7FunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         # chrome_options.add_argument('--dns-prefetch-disable')
#         # chrome_options.add_argument('--no-sandbox')        
#         # chrome_options.add_argument('--headless')
#         # chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#         super(Lab7FunctionalTest, self).setUp()
#         self.selenium.maximize_window()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab7FunctionalTest, self).tearDown()

#     def test_input_message(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         # selenium.get('http://ppw-f-andriansyah-lab7.herokuapp.com/')
#         selenium.get('http://localhost:8000/7th_story')
#         # find the form element
#         name = selenium.find_element_by_id('id_name')
#         message = selenium.find_element_by_id('id_messages')

#         submit = selenium.find_element_by_id('submit')

#         # Fill the form with data
#         name.send_keys('huahuahua')
#         time.sleep(1)
#         message.send_keys('Coba-coba')
#         time.sleep(1)

#         # submitting the form
#         submit.send_keys(Keys.RETURN)
#         time.sleep(5)
#         selenium.execute_script("window.scrollTo(0, document.body.scrollHeight);")
#         time.sleep(5)

#         # test "Coba-coba" exists in website
#         self.assertIn( "Coba-coba", self.selenium.page_source)

#     def test_layout_landing_page_has_header(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         # selenium.get('http://ppw-f-andriansyah-lab7.herokuapp.com/')
#         selenium.get('http://localhost:8000/7th_story')
#         #test website contains header
#         header = selenium.find_element_by_class_name('header').text
#         self.assertIn("Home", header)

#     def test_layout_landing_page_has_footer(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         # selenium.get('http://ppw-f-andriansyah-lab7.herokuapp.com/')
#         selenium.get('http://localhost:8000/7th_story')
#         #test website contains footer
#         footer = selenium.find_element_by_class_name('footer').text
#         self.assertIn("Muhammad Andriansyah Putra", footer)

#     def test_style_heading1(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         # selenium.get('http://ppw-f-andriansyah-lab7.herokuapp.com/')
#         selenium.get('http://localhost:8000/7th_story')
#         # test h1 using the right color
#         h1= selenium.find_element_by_tag_name('h1').value_of_css_property("color")
#         self.assertEquals("rgba(229, 0, 153, 1)", h1)

#     def test_style_text_has_the_right_color(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         # selenium.get('http://ppw-f-andriansyah-lab7.herokuapp.com/')
#         selenium.get('http://localhost:8000/7th_story')
#         #test footer text has the right color
#         foot = selenium.find_element_by_class_name('footer').value_of_css_property("color")
#         self.assertEquals("rgba(229, 0, 153, 1)", foot)

        
        

         



