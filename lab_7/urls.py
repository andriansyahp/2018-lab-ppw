from django.urls import path
from . import views

#url for app
urlpatterns = [
    path('', views.index_7, name='seventh'),
    path('status_send', views.status_send, name='status_send'),
]
