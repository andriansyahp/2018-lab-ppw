from django import forms
from .models import StatusMessage

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Put your name here...'
    }
    message_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'form-control',
        'placeholder':'What\'s Happening?'
    }

    name = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=name_attrs))
    message = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=message_attrs))

