from django.urls import path
from . import views

#url for app
urlpatterns = [
    path('', views.index_6, name='sixth'),
    path('status_submit', views.status_submit, name='status_submit'),
    path('about_me', views.about_me, name='about_me'),
]
