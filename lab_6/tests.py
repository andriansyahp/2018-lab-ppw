# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import index_6
# from .models import StatusMessage
# from .forms import Status_Form
# import unittest

# # Create your tests here.
# class Lab6UnitTest(TestCase):
    
#     def test_hello_page_is_exist(self):
#         response = Client().get('/6th_story')
#         self.assertEqual(response.status_code,200)

#     def test_using_index_func(self):
#         found = resolve('/6th_story')
#         self.assertEqual(found.func, index_6)

#     def test_index_contains_hello_apa_kabar(self):
#         response = Client().get('/6th_story')
#         html_response = response.content.decode('utf8')
#         self.assertIn("Hello", html_response)      
#         self.assertIn("Apa Kabar?", html_response)
    
#     def test_index_contains_form_to_send_status_message(self):
#         response = Client().get('/6th_story')
#         html_response = response.content.decode('utf8')
#         self.assertIn('<form class="post-form" method="POST" action="/6th_story/status_submit">', html_response)

#     def test_model_can_create_new_status(self):
#         # Creating a new message
#         new_status = StatusMessage.objects.create(name='budi', message='mengerjakan lab_6 ppw')

#         # Retrieving all available status messages
#         counting_all_available_status = StatusMessage.objects.all().count()
#         self.assertEqual(counting_all_available_status, 1)

#     def test_form_validation_for_blank_items(self):
#         form = Status_Form(data={'name': '', 'message': ''})
#         self.assertFalse(form.is_valid())
#         self.assertEqual(
#             form.errors['message'],
#             ["This field is required."]
#         )

#     def test_lab6_post_success(self):
#         test = 'Anonymous'
#         response_post = Client().post('/6th_story', {'name': test, 'message': test})
#         self.assertEqual(response_post.status_code, 200)

#     def test_lab6_about_me_page_is_exist(self):
#         response = Client().get('/6th_story/about_me')
#         self.assertEqual(response.status_code,200)

#     def test_lab6_about_me_page_contains_my_name(self):
#         response = Client().get('/6th_story/about_me')
#         html_response = response.content.decode('utf8')
#         self.assertIn("Muhammad Andriansyah Putra", html_response)      

#     def test_lab6_about_me_page_contains_my_age(self):
#         response = Client().get('/6th_story/about_me')
#         html_response = response.content.decode('utf8')
#         self.assertIn("19", html_response)

#     def test_lab6_about_me_implements_footer(self):
#         response = Client().get('/6th_story/about_me')
#         self.assertTemplateUsed(response, 'partials/footer.html')      

#     def test_lab6_about_me_implements_header(self):
#         response = Client().get('/6th_story/about_me')
#         self.assertTemplateUsed(response, 'partials/header.html') 
         



