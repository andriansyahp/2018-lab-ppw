from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
import requests, json
from .forms import Subscribe_Form
from .models import Subscribe
from django.core import serializers
# Create your views here.

response = {}

def homepage(request):
    return render(request, 'landing-page-lab10.html')

def subscribe(request):    
    html = 'subscribe.html'
    response['subscribe_form'] = Subscribe_Form()
    return render(request, html, response)
    
def check_email(request, email):
    if (Subscribe.objects.filter(email=email).count() > 0):
        return JsonResponse({'state': False})
    return JsonResponse({'state': True})

@csrf_exempt
def subscribe_submit(request):
    if(request.method == 'POST'):
        name = request.POST['name']
        email = request.POST['email']
        passwd = request.POST['password']
        subscription = Subscribe(name=name,email=email,password=passwd)
        subscription.save()
    return HttpResponse("OK")

def subscribers_data(request):
    subscribers = Subscribe.objects.all()
    subscribers_json = serializers.serialize('json', subscribers)
    return HttpResponse(subscribers_json, content_type='application/json')

def show_subscriber(request):
    return render(request, 'subscribers.html')

@csrf_exempt
def unsubscribe(request):
    obj = request.POST["id"]
    Subscribe.objects.filter(pk=obj).delete()
    return HttpResponse(json.dumps({"OK":obj}))




