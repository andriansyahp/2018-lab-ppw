from django.urls import path
from django.conf.urls import url
from . import views
#url for app
urlpatterns = [
    path('', views.homepage, name='tenth'),
    path('subscribe', views.subscribe, name='sub'),
    path('subscribe_submit', views.subscribe_submit, name='subscribe_submit'),
    path('subscriber_list', views.show_subscriber, name='subscribers'),
    path('subscriber_list/data', views.subscribers_data, name='subscribers_data'),
    path('subscriber_list/unsubscribe', views.unsubscribe, name='unsubscribe'),
    path('check_email/<str:email>', views.check_email, name='check_email'),
]
