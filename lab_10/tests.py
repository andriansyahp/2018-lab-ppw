# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import homepage
# from .views import subscribe
# from .views import check_email
# from .views import subscribe_submit
# from .models import Subscribe
# from .forms import Subscribe_Form
# import unittest


# # Create your tests here.
# class Lab10UnitTest(TestCase):
    
#     def test_hello_page_is_exist(self):
#         response = Client().get('/10th_story/')
#         self.assertEqual(response.status_code,200)

#     def test_page_contains_welcome_greetings(self):
#         response = Client().get('/10th_story/')
#         html_response = response.content.decode('utf8')
#         self.assertIn("Welcome", html_response)      
#         self.assertIn("Selamat Datang", html_response)
    
#     def test_index_contains_buttons_to_change_theme(self):
#         response = Client().get('/10th_story/')
#         html_response = response.content.decode('utf8')
#         self.assertIn('theme-select', html_response)

#     def test_page_contains_about_me_and_has_my_age(self):
#         response = Client().get('/10th_story/')
#         html_response = response.content.decode('utf8')
#         self.assertIn("Hello", html_response)      
#         self.assertIn("19", html_response)

#     def test_page_contains_cv(self):
#         response = Client().get('/10th_story/')
#         html_response = response.content.decode('utf8')
#         self.assertIn("Résumé", html_response)      

#     def test_page_contains_accordion(self):
#         response = Client().get('/10th_story/')
#         html_response = response.content.decode('utf8')
#         self.assertIn("accordion", html_response)    

#     def test_book_subscribe_page_exists(self):
#         response = Client().get('/10th_story/subscribe')
#         self.assertEqual(response.status_code,200)
    
#     def test_book_subscribe_page_contains_form(self):
#         response = Client().get('/10th_story/subscribe')
#         html_response = response.content.decode('utf8')
#         self.assertIn("form", html_response)

#     def test_json_state_of_email_status_page_exists(self):
#         response = Client().get('/10th_story/check_email/heyhey@gmail')
#         self.assertEqual(response.status_code,200)

#     def test_model_can_subscribe(self):
#         # Creating a new user
#         new_user = Subscribe.objects.create(name='budi', email='budi@yahoo.com', password="test123")

#         # Retrieving all available user subscribed
#         counting_all_available_user = Subscribe.objects.all().count()
#         self.assertEqual(counting_all_available_user, 1)


