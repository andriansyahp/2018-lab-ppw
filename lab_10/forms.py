from django import forms
from .models import Subscribe

class Subscribe_Form(forms.Form):
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id': 'id_name',
        'placeholder':'Put your name here...'
    }
    email_attrs = {
        'type': 'email',
        'class': 'form-control',
        'id': 'id_email',
        'placeholder':'Fill this box with your e-mail account.'
    }
    password_attrs = {
        'type': 'password',
        'class': 'form-control',
        'id': 'id_passwd',
        'placeholder':'Protect your subscription account with some password.'
    }
    reenterpasswd_attrs = {
        'type': 'password',
        'class': 'form-control',
        'id': 'id_reenterpasswd',
        'placeholder':'Re-enter your password.'
    }

    name = forms.CharField(label = 'Name', required=True, max_length=27, widget=forms.TextInput(attrs=name_attrs), help_text='100 characters max.')
    email = forms.CharField(label = 'E-mail', required=True, widget=forms.EmailInput(attrs=email_attrs), help_text='Make sure to enter a valid e-mail and use e-mail that never been subscribed before.')
    password = forms.CharField(label = 'Password', required=True, widget=forms.PasswordInput(attrs=password_attrs), help_text='Must contains 8 characters minimum.')
    reenterpasswd = forms.CharField(label = 'Re-enter Password', required=True, widget=forms.PasswordInput(attrs=reenterpasswd_attrs), help_text='Re-enter your password correctly.')


