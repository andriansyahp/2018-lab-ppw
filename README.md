# Tutorials and Assignment Repository

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2018/2019

* * *

## Welcome.

Welcome to the code repository of Muhammad Andriansyah Putra, a college student currently trying his best to excel in web programming.
His NPM is 1706044093.
This repository hosts weekly tutorial codes and other, such as course-related
code snippets.

## Pipeline Status
[![pipeline status](https://gitlab.com/andriansyahp/2018-lab-ppw/badges/master/pipeline.svg)](https://gitlab.com/andriansyahp/2018-lab-ppw/commits/master)

* * *

## Code Coverage Report
[![coverage report](https://gitlab.com/andriansyahp/2018-lab-ppw/badges/master/coverage.svg)](https://gitlab.com/andriansyahp/2018-lab-ppw/commits/master)

* * *

### Happy Coding :)
